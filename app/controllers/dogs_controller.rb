class DogsController < ApplicationController
  def index
    @dogs = Dog.all
  end

  def new
    @dog = Dog.new
  end

  def create
    dog = Dog.new(dog_params)

    return redirect_to dogs_url, notice: "Dogs uploaded." if dog.save
    render :new
  end

  def destroy
    @dog = Dog.find(params[:id])
    @dog.destroy
    redirect_to dogs_url, notice: "Dog successfully deleted."
  end

  private

  def dog_params
    params.require(:dog).permit(:id, :file, :name, :color, :size, :good_boy)
  end
end
