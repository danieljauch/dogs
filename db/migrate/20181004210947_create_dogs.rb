class CreateDogs < ActiveRecord::Migration[5.2]
  def change
    create_table :dogs do |t|
      t.string :name, null: false, default: "Fido"
      t.string :color, null: false, default: "Black"
      t.string :size, null: false, default: "Small"
      t.boolean :good_boy, null: false, default: true

      t.timestamps
    end
  end
end
