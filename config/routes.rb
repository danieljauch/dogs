Rails.application.routes.draw do
  resources :dogs, only: %i[index new create destroy]

  root "dogs#index"
end
